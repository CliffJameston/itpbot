import json
from track import *
from userlist import *

def ProcessArtists(unformattedArtistsList):
	# Takes an array of Artist objects
	# Returns an array of strings, using the names of artists
	artistList = []
	for attribute in unformattedArtistsList:
		for name, artist in attribute.items():
			artistList.append(artist)
	return artistList

def SearchSongs(searchString):
	found_count = 0
	for track in tracks:
		if track.HasArtist(searchString) == True:
			track.PrintTrack()
			found_count = found_count + 1
	print("Found " + str(found_count) + " songs.")

def FindDuplicates():
	found_count = 0
	found_songs = []
	for track in tracks:
		track_found = 0
		if track.name not in found_songs:
			for result_track in tracks:
				if track.name == result_track.name and track.artists == result_track.artists:
					track_found = track_found + 1
			if track_found > 1:
				found_songs.append(track.name)
				#track.PrintTrack()
				print(track.name + " by " + track.artistList + " occurs " + str(track_found) + " times.")
				#print(track.name + " " + str(track_found))
				found_count = found_count + 1
	print("Found " + str(found_count) + " duplicate songs.")


# Read in the saved playlist info
with open('playlist.json', encoding='utf8') as json_file:
	data = json.load(json_file)

# Set up array of tracks
tracks = []

# Temp variables
tempAddedAt = ""
tempAddedBy = ""
tempArtists = []
tempDurationMS =0
tempName = ""

# Iterate through the json data and create tracks
# The data's multi-leveled, so some of these have to be a little wonky
for track in data:
	# Loop through each Track object's key,value pairs
	# Added_At and Added_By are separate from the Track object, so they're easier to get
	for attribute, value in track.items():
		# Added_At is simple
		if attribute == 'added_at':
			tempAddedAt = value
		# Added_By is a User object, so we have to extract the ID value from it
		# Additionally, we just go ahead and find the match for it from our list
		elif attribute == 'added_by':
			for addedByAttribute, addedByValue in value.items():
				if addedByAttribute == 'id':
					tempAddedBy = FindUser(addedByValue)
		else:
			# After we've gone through Added_At and Added_By, everything else is part of the Track object
			# Now we have to look through *those* values
			for trackAttribute, trackValue in value.items():
				# Artists is a list of Artist objects, not a string.
				# We loop through each artist, adding its object to the array we'll pass for that parameter.
				if trackAttribute == 'artists':
					for artist in trackValue:
						tempArtists.append(artist)
				# Duration_MS is an int, the duration of the track in Milliseconds
				elif trackAttribute == 'duration_ms':
					tempDurationMS = trackValue
				# Name is just a string, so it passes easily as well.
				elif trackAttribute == 'name':
					tempName = trackValue
			# After all attributes have been extracted, process the array of Artist objects
			# and convert it to an array of strings of artist names
			artistParameter = ProcessArtists(tempArtists)
			# Create a new custom Track object, and add it to the end of our list of processed tracks
			tracks.append(Track(tempAddedAt, tempAddedBy, artistParameter, tempDurationMS, tempName))
			# Reset the array of Artists, since we just append to it higher up.
			tempArtists = []

# Setup is finished, tracks[] now has all songs loaded in and set up correctly
# After this, we can run data analysis and such.

FindDuplicates()
