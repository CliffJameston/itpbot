class Track:
	def __init__(self, added_at, added_by, artists, duration_ms, name):
		self.added_at = added_at
		self.added_by = added_by
		self.artists = artists
		self.artistList = self.ArtistsToString()
		self.duration_ms = duration_ms
		self.name = name
		self.durationString = self.ParseDuration()

	def ParseDuration(self):
		# Format Duration in MM:SS
		duration = self.duration_ms/1000
		durationMinutes = int(duration/60)
		durationSeconds = round((duration - (durationMinutes * 60)),2)
		if durationSeconds < 10:
			durationSeconds = "0" + str(durationSeconds) # Pad single-digit seconds values with a leading zero
		durationString = str(durationMinutes) + ":" + str(durationSeconds)[:5]
		return durationString

	def PrintTrack(self):
		# Setup

		# Print
		print(self.name)
		print("-----")
		print("Artists:  " + self.artistList)
		print("Duration: " + self.durationString)
		print("Added At: " + self.added_at)
		print("Added By: " + self.added_by)
		print()

	def HasArtist(self, artist_to_find):
		for artist in self.artists:
			if artist_to_find in artist:
				return True
		return False

	def ArtistsToString(self):
		# Format Artists
		artistList = ""
		for artist in self.artists:
			if artistList == "":
				artistList = artist
			else:
				artistList = artistList + ", " + artist
		return artistList
