import os
import discord
from dotenv import load_dotenv

# Client setup
load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
client = discord.Client()

@client.event
async def on_ready():
	print(f'I\'m all ready and waiting!')

client.run(TOKEN)