import csv

class User:
	def __init__ (self, id, name):
		self.id = id
		self.name = name

# Set up variables
UserList = []
tempID = ""
tempName = ""

# Load existing user list
with open('userlist.csv', 'r+', newline='') as csvfile:
	users = csv.reader(csvfile, delimiter=',')
	for row in users:
		# Each row is an 'id', 'name' pair, load in each one to the array
		for item in row:
			if tempID == "":
				tempID = item
			else:
				tempName = item
				UserList.append(User(tempID, tempName))
				tempID = ""

def AddUserDialog():
	# Prompt the user for the new user's info.
	# Intended to be run independently from the command line
	newID = input("Enter the ID of the user to add: ")
	newName = input("Enter the user name for that ID: ")

	AddUpdateUser(newID, newName)

def AddUpdateUser(id, name):
	# Update User if ID is already present in list
	# Otherwise, append new user to list and file
	for user in UserList:
		if user.id == id:
			user.name = name
			break
	UserList.append(User(id, name))
	with open('userlist.csv', 'a', newline='') as csvfile:
		userwriter = csv.writer(csvfile, delimiter=',')
		userwriter.writerow([id, name])

def FindUser(id):
	for user in UserList:
		if user.id == id:
			return user.name
	newName = input("User ID not found, enter the name associated with the ID \'" + id + "\': ")
	AddUpdateUser(id, newName)
	return newName